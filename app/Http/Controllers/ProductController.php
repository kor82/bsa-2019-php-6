<?php

namespace App\Http\Controllers;

use App\Http\Presenter\ProductArrayPresenter;
use App\Action\Product\GetCheapestProductsAction;

class ProductController extends Controller
{

	protected $presenter;

	public function __construct(
		ProductArrayPresenter $presenter,
		GetCheapestProductsAction $action
	)
	{
		$this->presenter = $presenter;
		$this->action = $action;
	}

	/**
	 * Get cheapest products
	 * 
	 * @return array of App\Entity\Product
	 */
	public function getCheapestProducts()
	{
		$response = $this->action->execute();

		$products = $this->presenter->presentCollection($response->getProducts());

	    return view('cheap_products', ['products' => $products]);
	}

}