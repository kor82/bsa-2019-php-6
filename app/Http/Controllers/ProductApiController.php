<?php

namespace App\Http\Controllers;

use App\Entity\Product;
use App\Http\Presenter\ProductArrayPresenter;
use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetMostPopularProductAction;

class ProductApiController extends Controller
{

    protected $presenter;

    public function __construct(ProductArrayPresenter $presenter)
    {
        $this->presenter = $presenter;
    }

    /**
     * Get all products
     *
     * @param  GetAllProductsAction $action
     * @return \Illuminate\Http\Response
     */
    public function index(GetAllProductsAction $action)
    {
        $response = $action->execute();

        $products = $this->presenter->presentCollection($response->getProducts());

        return response()->json($products);
    }

    /**
     * Get most popular product
     * 
     * @param  GetMostPopularProductAction $action
     * @return \Illuminate\Http\Response
     */
    public function popular(GetMostPopularProductAction $action)
    {
        $response = $action->execute();

        $product = $this->presenter->present($response->getProduct());

        return response()->json($product);
    }    

}
