<?php

declare(strict_types=1);

namespace App\Repository;

class ProductRepository implements ProductRepositoryInterface
{

	protected $products;

    /**
     * @param Product[] $products
     */
    public function __construct(array $products)
    {
    	$this->products = $products;
    }

    /**
     * @return Product[]
     */
    public function findAll(): array
    {
    	return $this->products;
    }
}
