<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;
use App\Entity\Product;


class GetMostPopularProductAction
{
    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetMostPopularProductResponse
    {
        $products = $this->repository->findAll();

        $mostPopular = $this->getMostPopular($products);
        
        return new GetMostPopularProductResponse($mostPopular);
    }

	protected function getMostPopular(array $products) : Product
    {
		return collect($products)->sortBy(function($product) {
									  return $product->getRating();
									})
								 ->last();
    }    
}