<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }
    
    public function execute(): GetCheapestProductsResponse
    {
        $products = $this->repository->findAll();

        $cheapest = $this->getCheapest($products);
        
        return new GetCheapestProductsResponse($cheapest);
    }

    protected function getCheapest(array $products, $qty = 3) : array
    {
		return collect($products)->sortBy(function($product) {
									  return $product->getPrice();
									})
								 ->take($qty)
                                 ->values()
                                 ->all();
    }
}