<?php

declare(strict_types=1);

namespace App\Entity;

class Product
{
	protected $id;
	protected $name;
	protected $price;
	protected $imageUrl;
	protected $rating;

	public function __construct($id, $name, $price, $imageUrl, $rating)
	{
		$this->id = $id;
		$this->name = $name;
		$this->price = $price;
		$this->imageUrl = $imageUrl;
		$this->rating = $rating;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function getImageUrl()
	{
		return $this->imageUrl;
	}

	public function getRating()
	{
		return $this->rating;
	}		

}