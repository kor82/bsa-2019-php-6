<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Product;

class ProductGenerator
{
    /**
     * @return Product[]
     */
    public static function generate(): array
    {
        return [
        	new Product(1, 'bmw', '200', 'bmw_img', 4.8),
        	new Product(2, 'wv', '180', 'wv_img', 4.7),
        	new Product(3, 'skoda', '140', 'skoda_img', 4.3),
        	new Product(4, 'opel', '130', 'opel_img', 3.8),
        	new Product(5, 'mini', '170', 'mini_img', 4.2),
        	new Product(6, 'mercedes', '210', 'mercedes_img', 4.9),
        	new Product(7, 'volvo', '150', 'volvo_img', 4.1),
        	new Product(8, 'mazda', '160', 'mazda_img', 4.5),
        	new Product(9, 'toyota', '190', 'toyota_img', 4.6),
        	new Product(10, 'lexus', '220', 'lexus_img', 5.0),
        ];
    }
}