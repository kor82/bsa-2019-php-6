<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ProductGenerator;
use App\Repository\ProductRepository;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repository\ProductRepositoryInterface',
            function() {
                return new ProductRepository(ProductGenerator::generate());
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
